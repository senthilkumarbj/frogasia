<?php

return [

    'BREEDING_POND_CNT'     => 2,
    'BREEDING_FROG_PAIR'    => 3,
    'FROG_BIRTH'            => 3,

    'LAYING_EGG_MIN'        => 2000,
    'LAYING_EGG_MAX'        => 12000,

    'TADPOLE_FROG_CNT'      => 5,
    'ADULT_FROG_CNT'        => 5,

    'POND_PAGINATE'         => 30,
    'FROG_PAGINATE'         => 30,
    'BREEDING_PAGINATE'     => 30,

    'FROG_DEATH_CNT'        => 5,
    'FROG_INFECTION_CNT'    => 5,

    'POND_CLEANING_CNT'     => 1,

] ;
