<?php

use Illuminate\Foundation\Testing\WithoutMiddleware ;
use Illuminate\Foundation\Testing\DatabaseMigrations ;
use Illuminate\Foundation\Testing\DatabaseTransactions ;


class FrogTest extends TestCase {


    /*
    * FrogTest::testAllFrogs()
    *
    * It test gets all frogs action
    */
    public function testAllFrogs() {

        printf( "\nAll Frogs to Display\n" ) ;
        $objResponse 	= $this->action( 'GET', 'FrogController@getFrogs' ) ;
        $this->assertResponseOk() ;
        $this->assertViewHas( 'frogs' ) ;
        $arrView 		= $objResponse->original;
        $this->assertTrue( ( count( $arrView[ 'frogs' ] ) ? true : 'No Frog available' ) ) ;

    }	// End of testAllFrogs()


    /*
    * FrogTest::testFrogBirth()
    *
    * It test frog details page
    */
    public function testDisplaysFrogDetails() {

        printf( "\nFrog Details to Display\n" ) ;
        $objResponse 	= $this->action( 'GET', 'FrogController@frogDetails', [ 'id' => 1 ] ) ;
        $this->assertResponseOk() ;
        $this->assertViewHas( 'frog' ) ;
        $arrView 		= $objResponse->original;
        $this->assertTrue( ( count( $arrView[ 'frog' ] ) ? true : 'No Frog available' ) ) ;
		printf( "\n -- " . json_encode( $arrView['frog'] ) . " -- \n" ) ;

    }	// 	End of testDisplaysFrogDetails()


    /*
    * FrogTest::testFrogBirth()
    *
    * It test frog birth action
    */
    public function testFrogBirth() {

        printf( "\nFrog Birth\n" ) ;
        $objResponse 	= $this->action( 'GET', 'FrogController@frogBirth' ) ;
        $this->assertResponseOk() ;
        $this->assertViewHas( 'arrResult' ) ;
        $arrView 		= $objResponse->original;
        $this->assertTrue( ( $arrView ) ? true : 'No Frog for birth' ) ;
		printf( "\n -- " . $arrView['arrResult']['msg'] . " -- \n" ) ;

    }	// 	End of testFrogBirth()


    /*
    * FrogTest::testTadpoleDevelopment()
    *
    * It test tadpole to frog development action
    */
    public function testTadpoleDevelopment() {

        printf( "\nTadpole Development\n" ) ;
        $objResponse 	= $this->action( 'GET', 'FrogController@tadpoleDevelopment' ) ;
        $this->assertResponseOk() ;
        $this->assertViewHas( 'arrResult' ) ;
        $arrView 		= $objResponse->original;
        $this->assertTrue( ( $arrView ) ? true : 'No tadpole for frog development' ) ;
		printf( "\n -- " . $arrView['arrResult']['msg'] . " -- \n" ) ;

    }	// 	End of testTadpoleDevelopment()


}	//	End of class - FrogTest { }
