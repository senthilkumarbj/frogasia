<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group( [ 'middleware' => [ 'web' ] ], function () {

    Route::get( '/', [ 'as'=>'home' , 'uses'=>'HomeController@homePage' ] ) ;

    Route::get( 'breedings', [ 'as'=>'breedings' , 'uses'=>'BreedingController@getBreedings' ] ) ;
    Route::get( 'breeding/make', ['as'=>'makeBreeding' , 'uses'=>'BreedingController@makeBreeding'] ) ;

    Route::get( 'frogs', [ 'as'=>'frogs' , 'uses'=>'FrogController@getFrogs' ] ) ;
    Route::get( 'frog/adultdevelopment', [ 'as'=>'adultDevelopment' , 'uses'=>'FrogController@adultDevelopment' ] ) ;
    Route::get( 'frog/birth', [ 'as'=>'frogBirth' , 'uses'=>'FrogController@frogBirth' ] ) ;
    Route::get( 'frog/death', [ 'as'=>'frogDeath' , 'uses'=>'FrogController@frogDeath' ] ) ;
    Route::get( 'frog/infection', [ 'as'=>'frogInfection' , 'uses'=>'FrogController@frogInfection' ] ) ;

    Route::get( 'ponds', [ 'as'=>'ponds' , 'uses'=>'PondsController@getPonds' ] ) ;
    Route::get( 'pond/cleaning', [ 'as'=>'pondCleaning' , 'uses'=>'PondsController@pondCleaning' ] ) ;

    Route::get( 'tadpole/development', [ 'as'=>'tadpoleDevelopment' , 'uses'=>'FrogController@tadpoleDevelopment' ] ) ;

    Route::get( 'breeding/{id}', [ 'as'=>'breedingdetail' , 'uses'=>'BreedingController@BreedingDetails' ] ) ;
    Route::get( 'pond/{id}', [ 'as'=>'ponddetail' , 'uses'=>'PondsController@pondDetails' ] ) ;
    Route::get( 'frog/{id}', [ 'as'=>'frogdetail' , 'uses'=>'FrogController@frogDetails' ] ) ;

} ) ;
