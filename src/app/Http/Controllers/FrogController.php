<?php

namespace App\Http\Controllers ;

use Illuminate\Http\Request ;
use Exception ;
use Log ;

use \App\Models\Frog ;

class FrogController extends Controller {


    /*
    * FrogController::getFrogs()
    *
    * It gets all frogs with pagination to list it
    */
    public function getFrogs( Request $request ) {

        $frogs  = Frog::orderBy('updated_at', 'desc')
                    ->paginate( config( 'config.FROG_PAGINATE' ) ) ;
        return View( 'layouts.frogs', compact( 'frogs' ) ) ;

    }   // End of FrogController::getFrogs()


    /*
    * FrogController::frogDetails()
    *
    * It gets all details of frog to display
    */
    public function frogDetails( Request $request, $id ) {

        $frog   = Frog::where( 'frog_id', $id )->first() ;
        return View( 'layouts.frog', compact( 'frog' ) ) ;

    }   // End of FrogController::frogDetails()


    /*
    * FrogController::frogBirth()
    *
    * This function does birth of tadpole from eggs which are 7 to 10 days old
    */
    public function frogBirth( Request $request ) {

        try {

            // First need to get random breeding for tadpole birth which are 7 to 10 days old
            $objFrog            = new \App\Models\Frog ;
            $arrFrog            = $objFrog->getBreedingForFrogBirth( config( 'config.FROG_BIRTH' ) ) ;
            $strResult          = 'Frog birth was failure' ;
            if ( count( $arrFrog ) > 0 ) {

                if ( $objFrog->saveFrogBirth( $arrFrog ) ) {

                    $strResult  = 'Frog birth was successful' ;
                }
            }
            elseif ( 0 == count( $arrFrog ) ) {

                $strResult      = 'No Frog for birth' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'FrogController::frogBirth() - ' . $e->getMessage() ) ;
            $strResult          = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult              = [ 'head' => 'Frog Birth', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of FrogController::frogBirth()


    /*
    * FrogController::tadpoleDevelopment()
    *
    * This function does from tadpole to frog which are 12+ weeks old
    */
    public function tadpoleDevelopment( Request $request ) {

        try {

            // It updates fully grown tadple as frog which are around 12+ weeks old
            $objFrog                = new \App\Models\Frog ;
            $mixedResult            = $objFrog->tadpoleToFrogDevelopment( config( 'config.TADPOLE_FROG_CNT' ) ) ;
            if ( $mixedResult > 0 ) {

                $strResult          = 'Tadpole to Frog development was successful' ;
            }
            elseif ( $mixedResult == 0 )  {

                $strResult          = 'No tadpole to develop into Frog' ;
            }
            elseif ( $mixedResult === false )  {

                $strResult          = 'Tadpole to Frog development was failure' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'FrogController::tadpoleDevelopment() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Tadpole Development', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of FrogController::tadpoleDevelopment()


    /*
    * FrogController::adultDevelopment()
    *
    * It updates 3+ year old frog as adult which mean ready to breed
    */
    public function adultDevelopment( Request $request ) {

        try {

            // It updates 3+ year old frog as adult which mean ready to breed
            $objFrog                = new \App\Models\Frog ;
            $mixedResult            = $objFrog->adultDevelopment( config( 'config.ADULT_FROG_CNT' ) ) ;
            if ( $mixedResult > 0 ) {

                $strResult          = 'Frog adult development was successful' ;
            }
            elseif ( $mixedResult == 0 )  {

                $strResult          = 'No frog to develop into adult' ;
            }
            elseif ( $mixedResult === false )  {

                $strResult          = 'Frog adult development was failure' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'FrogController::adultDevelopment() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Frog Adult Development', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of FrogController::adultDevelopment()


    /*
    * FrogController::frogDeath()
    *
    * It updates frog death with cause in random
    */
    public function frogDeath( Request $request ) {

        try {

            // It updates frog death with cause in random
            $objFrog                = new \App\Models\Frog ;
            $mixedResult            = $objFrog->frogDeath( config( 'config.FROG_DEATH_CNT' ) ) ;
            if ( $mixedResult > 0 ) {

                $strResult          = 'Frog / Tadpole death was successful' ;
            }
            elseif ( $mixedResult == 0 )  {

                $strResult          = 'No Frog / Tadpole to die' ;
            }
            elseif ( $mixedResult === false )  {

                $strResult          = 'Frog / Tadpole death was failure' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'FrogController::frogDeath() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Frog Death', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of FrogController::frogDeath()


    /*
    * FrogController::frogInfection()
    *
    * It updates frog with infection
    */
    public function frogInfection( Request $request ) {

        try {

            // It updates frog with infection
            $objFrog                = new \App\Models\Frog ;
            $mixedResult            = $objFrog->frogInfection( config( 'config.FROG_INFECTION_CNT' ) ) ;
            if ( $mixedResult > 0 ) {

                $strResult          = 'Frog Infection was successful' ;
            }
            elseif ( $mixedResult == 0 )  {

                $strResult          = 'No frog to Infect' ;
            }
            elseif ( $mixedResult === false )  {

                $strResult          = 'Frog Infection was failure' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'FrogController::frogInfection() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Frog Infection', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of FrogController::frogInfection()


}   // End of class - FrogController { }
