<?php

namespace App\Http\Controllers ;

use Illuminate\Http\Request ;
use Exception ;
use Log ;

use \App\Models\Breeding ;
use \App\Models\Pond ;

class BreedingController extends Controller {


    /*
    * BreedingController::getBreedings()
    *
    * It gets all breedings with pagination to list it
    */
    public function getBreedings( Request $request ) {

        $breedings  = Breeding::orderBy('updated_at', 'desc')
                        ->paginate( config( 'config.BREEDING_PAGINATE' ) ) ;
        return View( 'layouts.breedings', compact( 'breedings' ) ) ;

    }   // End of BreedingController::getBreedings()


    /*
    * BreedingController::breedingDetails()
    *
    * It gets all details of breeding to display
    */
    public function breedingDetails( Request $request, $id ) {

        $breeding   = Breeding::where( 'breeding_id', $id )->first() ;
        return View( 'layouts.breeding', compact( 'breeding' ) ) ;

    }   // End of BreedingController::breedingDetails()


    /*
    * BreedingController::makeBreeding()
    *
    * It makes breeding from random pond and frog
    */
    public function makeBreeding() {

        try {

            // Getting some random pond
            $objPond                = new \App\Models\Pond ;
            $strPond                = $objPond->getPondForBreeding( config( 'config.BREEDING_POND_CNT' ) ) ;
            $strResult              = '' ;
            if ( !empty( $strPond ) ) {

                // Getting some random adult frog from selected pond
                $objFrog            = new \App\Models\Frog ;
                $arrBreedingPair    = $objFrog->getFrogPair( $strPond, config( 'config.BREEDING_FROG_PAIR' ) ) ;
                if ( count( $arrBreedingPair ) > 0 ) {

                    // Creating breeding from selected frog
                    $objBreeding    = new \App\Models\Breeding ;
                    if ( $objBreeding->makeBreeding( $arrBreedingPair ) ) {

                        $strResult  = 'Breeding was successfully done' ;
                    }
                    else {

                        $strResult  = 'Breeding was not done' ;
                    }
                }
                else {

                    $strResult      = 'No frog pair(s) available' ;
                }
            }
            else {

                $strResult          = 'No pond(s) available or All ponds are full' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'BreedingController::makeBreeding() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Make Breeding', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of BreedingController::makeBreeding()


}   // End of class - BreedingController { }
