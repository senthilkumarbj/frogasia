<?php

namespace App\Http\Controllers ;

use Illuminate\Http\Request ;
use Exception ;
use Log ;

use \App\Models\Pond ;

class PondsController extends Controller {


    /*
    * PondsController::getPonds()
    *
    * It gets all ponds with pagination to list it
    */
    public function getPonds( Request $request ) {

        $ponds  = Pond::orderBy('updated_at', 'desc')
                    ->paginate( config( 'config.POND_PAGINATE' ) ) ;
        return View( 'layouts.ponds', compact( 'ponds' ) ) ;

    }   // End of PondsController::getPonds()


    /*
    * PondsController::pondDetails()
    *
    * It gets all details of pond to display
    */
    public function pondDetails( Request $request, $id ) {

        $pond   = Pond::where( 'pond_id', $id )->first() ;
        return View( 'layouts.pond', compact( 'pond' ) ) ;

    }   // End of PondsController::pondDetails()


    /*
    * PondsController::pondCleaning()
    *
    * It updates pond cleaning with cleaned date
    */
    public function pondCleaning( Request $request ) {

        try {

            // It updates pond cleaning with cleaned date
            $objPond                = new \App\Models\Pond ;
            $mixedResult            = $objPond->pondCleaning( config( 'config.POND_CLEANING_CNT' ) ) ;
            if ( $mixedResult > 0 ) {

                $strResult          = 'Pond cleaning was successful' ;
            }
            elseif ( $mixedResult == 0 )  {

                $strResult          = 'No Pond to cleaning / Last cleaning date is not more than a month' ;
            }
            elseif ( $mixedResult === false )  {

                $strResult          = 'Pond cleaning was failure' ;
            }
        }
        catch( Exception $e ) {

            Log::debug( 'PondsController::pondCleaning() - ' . $e->getMessage() ) ;
            $strResult              = 'Oops! Systen dowm, come back later.' ;
        }
        $arrResult                  = [ 'head' => 'Pond Cleaning', 'msg' => $strResult ] ;
        return View( 'layouts.cronresult', compact( 'arrResult' ) ) ;

    }   // End of PondsController::pondCleaning()


}   // End of class - PondsController { }
