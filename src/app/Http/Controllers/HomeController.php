<?php

namespace App\Http\Controllers ;

use Illuminate\Http\Request ;
use Exception ;

class HomeController extends Controller {


    /*
    * HomeController::homePage()
    *
    * It displays the home page
    */
    public function homePage( Request $request ) {

        // It fetch the home template
        return View( 'layouts.home' ) ;

    }   // End of HomeController::homePage()


}   // End of class - HomeController { }
