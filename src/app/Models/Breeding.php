<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB ;
use Log ;

class Breeding extends Model {

    protected   $table      = 'frog_breeding' ;
    protected   $primaryKey = 'breeding_id' ;
    public      $timestamps = false ;


    /*
    * Breeding::makeBreeding()
    * It makes breeding and stores it in database
    *
    * @ Param  : $arrBreedingPair   - Array of pair of adult frog(s)
    * @ Return : mixedResult        - true on success / false on failure
    */
    public function makeBreeding( $arrBreedingPair) {

        try {

            if ( !empty( $arrBreedingPair ) ) {

                if ( Breeding::insert( $arrBreedingPair ) ) {

                    return true ;
                }
            }
            return false ;
        }
        catch( Exception $e ) {

            Log::debug( 'Breeding::makeBreeding() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Breeding::makeBreeding()


}   // End of class - Breeding { }
