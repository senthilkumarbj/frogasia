<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB ;
use Log ;

class Pond extends Model {

    protected   $table      = 'pond_master' ;
    protected   $primaryKey = 'pond_id' ;
    public      $timestamps = false ;


    /*
    * Pond::getPondForBreeding()
    * It gets pond for breeding
    *
    * @ Param  : $intPondCnt - Number of pond requested
    * @ Return : mixedResult - string of pond id(s) on success / false on failure or no pond(s)
    */
    public function getPondForBreeding( $intPondCnt = 1 ) {

        try {

            /* Query Explanation
            * It filters ponds which has capacity available in order by last updated to recent updated
            * with requested number of ponds */
            $strQuery
                = ' SELECT
                        SUBSTRING_INDEX(
                            GROUP_CONCAT(
                                CASE WHEN pond_capacity > total_count THEN pond_id END
                                ORDER BY updated_at ASC
                            ), ",", ?
                        ) as pond_id
                    FROM pond_master ' ;

            $arrPond        = DB::select( $strQuery, [ $intPondCnt ] ) ;
            return
                ( count( $arrPond ) > 0 )
                    ? $arrPond[0]->pond_id
                    : false ;
        }
        catch( Exception $e ) {

            Log::debug( 'Pond::getPondForBreeding() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   //  End of Pond::get


    /*
    * Pond::pondCleaning()
    * It cleans the pond & updates with cleaned date also
    *
    * @ Param  : $intPondCnt - Number of pond requested
    * @ Return : mixedResult - No. of cleaned pond(s) on success / false on failure or no pond(s)
    */
    public function pondCleaning( $intPondCnt = 1 ) {

        try {

            /* Query Explanation
            * It updates pond with last cleaning date and
            * last cleaned date or created date should be more than a month */
            $strQuery
                = ' UPDATE pond_master SET
                        last_cleaned_date = NOW()
                    WHERE
                        last_cleaned_date < DATE_SUB( CURDATE(), INTERVAL 1 MONTH ) OR
                        ( last_cleaned_date IS NULL AND created_at < DATE_SUB( CURDATE(), INTERVAL 1 MONTH ) )
                    LIMIT ? ' ;

            return
                DB::update( $strQuery, [ $intPondCnt ] ) ;
        }
        catch( Exception $e ) {

            Log::debug( 'Pond::getPondForBreeding() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   //  End of Pond::getPondForBreeding()


}   // End of class - Pond { }
