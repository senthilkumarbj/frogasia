<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB ;
use Log ;

class Frog extends Model {

    protected   $table      = 'frog_master' ;
    protected   $primaryKey = 'frog_id' ;
    public      $timestamps = false ;


    /*
    * Frog::getFrogPairByPond()
    * It gets pair for breeding by pond
    *
    * @ Param  : $strPond    - Selected Pond IDs
    * @ Param  : $intPairCnt - Number of pair requested
    * @ Return : mixedResult - array of pair id(s) on success / false on failure or no pair(s)
    */
    public function getFrogPair( $strPond, $intPairCnt = 1 ) {

        try {

            /* Query Explanation
            * It filters frog pair which can reproduce & in order by last updated to recent updated
            * with requested number of pairs */
            $strQuery
                = '
                    SELECT
                        pond_id,
                        SUBSTRING_INDEX(
                            GROUP_CONCAT( CASE WHEN frog_gender = "M" THEN frog_id END ORDER BY updated_at ASC ), ",", %d
                        ) as male_id,
                        SUBSTRING_INDEX(
                            GROUP_CONCAT( CASE WHEN frog_gender = "F" THEN frog_id END ORDER BY updated_at ASC ), ",", %d
                        ) as female_id

                    FROM frog_master
                    WHERE pond_id IN ( %s ) AND ready_to_breed = "Y" AND is_alive = "Y"
                    GROUP BY pond_id
                ' ;
            $strQuery           = sprintf( $strQuery, $intPairCnt, $intPairCnt, $strPond ) ;
            $arrPondFrogs       = DB::select( $strQuery ) ;
            $arrFrogPair        = [] ;
            if ( ( count( $arrPondFrogs ) > 0 ) ) {

                // foreach prepares the pair in array format, so that it can be save by eloquent ORM method
                foreach( $arrPondFrogs as $eachPond ) {

                    $arrMale    = explode( ',', $eachPond->male_id ) ;
                    $arrFemale  = explode( ',', $eachPond->female_id ) ;
                    $cntMale    = count( $arrMale ) ;
                    $cntFemale  = count( $arrFemale ) ;
                    $intCnt     = ( $cntMale <= $cntFemale )
                                    ? $cntMale
                                    : $cntFemale ;

                    if ( $intCnt > 0 ) {

                        for( $i = 0 ; $i < $intCnt ; $i++ ) {

                            $arrFrogPair[]
                                = [
                                    'pond_id'           => $eachPond->pond_id ,
                                    'male_frog_id'      => $arrMale[$i] ,
                                    'female_frog_id'    => $arrFemale[$i],
                                    'no_of_eggs'        => rand(
                                                                config( 'config.LAYING_EGG_MIN' ),
                                                                config( 'config.LAYING_EGG_MAX' )
                                                            ),
                                ] ;
                        }
                    }
                }
            }
            return $arrFrogPair ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::getFrogPairByPond() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::getFrogPairByPond()


    /*
    * Frog::getBreedingForFrogBirth()
    * It gets breeding for frog birth
    *
    * @ Param  : $intBreedingCnt    - Number of Breeding requested
    * @ Return : mixedResult        - array of breeding details on success / false on failure or no breeding(s)
    */
    public function getBreedingForFrogBirth( $intBreedingCnt = 1 ) {

        try {

            /* Query Explanation
            * It filters frog pair which can reproduce & in order by last updated to recent updated
            * with requested number of pairs */
            $strQuery
                = '
                    SELECT
                        breeding_id, pond_id,
                        ROUND( ( no_of_eggs * 0.1 ) + ( no_of_eggs * 0.9 ) * RAND() ) AS birthCnt
                    FROM frog_breeding
                    WHERE
                        egg_developed = "N" AND
                        ( DATE( date_of_lay )
                            BETWEEN DATE( DATE_SUB( NOW(), INTERVAL 10 DAY ) ) AND
                            DATE( DATE_SUB( NOW(), INTERVAL 7 DAY ) ) )
                    ORDER BY updated_at LIMIT ?
                ' ;

            return
                DB::select( $strQuery, [ $intBreedingCnt ] ) ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::getBreedingForFrogBirth() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::getBreedingForFrogBirth()


    /*
    * Frog::saveFrogBirth()
    * It saves the frog birth
    *
    * @ Param  : $arrFrog       - Array of selected breeding details for Frog Birth
    * @ Return : mixedResult    - true(bool) on success / false(bool) on failure
    */
    public function saveFrogBirth( $arrFrog ) {

        try {

            if ( count( $arrFrog ) > 0 ) {

                $arrGender              = [ 'M', 'F' ] ;
                $arrBreeding            = [] ;
                $arrPond                = [] ;
                // foreach for every breeding
                foreach( $arrFrog as $eachFrog ) {

                    $arrNewFrog         = [] ;
                    // for-loop to create array of frog as required by birth count
                    for( $i = 0 ; $i < $eachFrog->birthCnt ; $i++ ) {

                        $arrNewFrog[]   = [
                                            'pond_id'       => $eachFrog->pond_id,
                                            'breeding_id'   => $eachFrog->breeding_id,
                                            'frog_name'     => substr( md5( rand() ), 0, 7 ),
                                            'frog_gender'   => $arrGender[ mt_rand( 0, 1 ) ],
                                        ] ;
                    }
                    // Inserts into table
                    if ( Frog::insert( $arrNewFrog ) ) {

                        $arrBreeding[]  = $eachFrog->breeding_id ;
                    }
                }
                if ( count( $arrBreeding ) >  0 ) {

                    /* Query Explanation
                    * Updating breeding as developed into tadpole/frog birth */
                    $strQuery
                        = sprintf(
                            ' UPDATE frog_breeding SET egg_developed = "Y" WHERE breeding_id IN ( %s ) ',
                            implode( ',', $arrBreeding )
                        ) ;
                    DB::update( $strQuery ) ;
                }
                $this->dataIntegrity() ;
                return true ;
            }
            return false ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::saveFrogBirth() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::saveFrogBirth()


    /*
    * Frog::tadpoleToFrogDevelopment()
    * It updates fully grown tadple as frog which are around 12 weeks ( +/- few days ) old
    *
    * @ Param  : $intTadpoleCnt - Number of Tadpole requested to change as frog
    * @ Return : mixedResult    - No.of updated records on success / false(bool) on failure
    */
    public function tadpoleToFrogDevelopment( $intTadpoleCnt = 1 ) {

        try {

            /* Query Explanation
            * It updates random tadpoles as frog with date of development of tadpole in order by DOB
            * with reuested no. of tadpole(s) */
            $strQuery       = ' UPDATE frog_master SET
                                    now_as = "FROG", dod_tadpole = NOW()
                                WHERE
                                    is_alive = "Y" AND now_as = "TADPOLE" AND
                                    date_of_birth < DATE_SUB( CURDATE(), INTERVAL 70 DAY )
                                ORDER BY date_of_birth ASC
                                LIMIT ? ' ;

            $mixedResult    = DB::update( $strQuery, [ $intTadpoleCnt ] ) ;
            $this->dataIntegrity() ;
            return $mixedResult ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::tadpoleToFrogDevelopment() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::tadpoleToFrogDevelopment()


    /*
    * Frog::adultDevelopment()
    * It updates 3+ year old frog as adult which mean ready to breed
    *
    * @ Param  : $intFrogCnt    - Number of Tadpole requested to change as frog
    * @ Return : mixedResult    - Number of updated record on success / false(bool) on failure
    */
    public function adultDevelopment( $intFrogCnt = 1 ) {

        try {

            /* Query Explanation
            * It updates random tadpoles as frog with date of development of tadpole in order by DOB
            * with reuested no. of tadpole(s) */
            $strQuery       = ' UPDATE frog_master SET
                                    ready_to_breed = "Y"
                                WHERE
                                    is_alive = "Y" AND now_as = "FROG" AND ready_to_breed = "N" AND
                                    date_of_birth < DATE_SUB( CURDATE(), INTERVAL 36 MONTH )
                                ORDER BY date_of_birth ASC
                                LIMIT ? ' ;

            return
                DB::update( $strQuery, [ $intFrogCnt ] ) ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::adultDevelopment() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::adultDevelopment()


    /*
    * Frog::frogDeath()
    * It updates frog death with cause in random and natural death
    *
    * @ Param  : $intFrogCnt    - Number of frog requested to make death
    * @ Return : mixedResult    - Number of updated record on success / false(bool) on failure
    */
    public function frogDeath( $intFrogCnt = 1 ) {

        try {

            /* Query Explanation
            * It updates frog with natural death who lives more than 8 years */
            $strQuery       = ' UPDATE frog_master SET
                                    is_alive = "N", date_of_death = NOW(), cause_of_death = "NATURAL"
                                WHERE
                                    is_alive = "Y" AND
                                    date_of_birth < DATE_SUB( CURDATE(), INTERVAL 8 YEAR )
                                ORDER BY date_of_birth ASC ' ;
            DB::update( $strQuery ) ;

            $arrColumns     = [ 'frog_id', 'pond_id', 'breeding_id', 'frog_name', 'date_of_birth',
                                'dod_tadpole', 'created_at', 'updated_at' ] ;
            $arrBy          = [ 'ASC', 'DESC' ] ;

            /* Query Explanation
            * It updates frog with cause of death random with requested no. of frog(s) */
            $strQuery       = sprintf(
                                '   UPDATE frog_master SET
                                        is_alive = "N", date_of_death = NOW(),
                                        cause_of_death = CASE FLOOR( RAND() * 5 )
                                                            WHEN 0 THEN "INFECTED"
                                                            WHEN 1 THEN "PREDATOR"
                                                            WHEN 2 THEN "PREDATOR"
                                                            WHEN 3 THEN "DISEASE"
                                                            WHEN 4 THEN "PREDATOR"
                                                        END
                                    WHERE
                                        is_alive = "Y"
                                    ORDER BY
                                        %s %s
                                    LIMIT
                                        %d ',
                                $arrColumns[ rand( 0, 7 ) ], $arrBy[ rand( 0, 1 ) ], $intFrogCnt
                            ) ;

            $mixedResult    = DB::update( $strQuery ) ;
            $this->dataIntegrity() ;
            return $mixedResult ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::frogDeath() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::frogDeath()


    /*
    * Frog::frogInfection()
    * It updates frog(s) with infection in random
    *
    * @ Param  : $intFrogCnt    - Number of frog requested to make infected
    * @ Return : mixedResult    - Number of updated record on success / false(bool) on failure
    */
    public function frogInfection( $intFrogCnt = 1 ) {

        try {

            $arrColumns     = [ 'frog_id', 'pond_id', 'breeding_id', 'frog_name', 'date_of_birth',
                                'dod_tadpole', 'created_at', 'updated_at' ] ;
            $arrBy          = [ 'ASC', 'DESC' ] ;

            /* Query Explanation
            * It updates frog with infection random with requested no. of frog(s) */
            $strQuery       = sprintf(
                                '   UPDATE frog_master SET healthy = "N"
                                    WHERE is_alive = "Y" AND healthy = "Y"
                                    ORDER BY %s %s LIMIT %d ',
                                $arrColumns[ rand( 0, 7 ) ], $arrBy[ rand( 0, 1 ) ], $intFrogCnt
                            ) ;

            $mixedResult    = DB::update( $strQuery ) ;
            $this->dataIntegrity() ;
            return $mixedResult ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::frogInfection() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::frogInfection()



    /*
    * Frog::dataIntegrity()
    *
    * It updates frog(s) with infection in random
    */
    public function dataIntegrity() {

        try {

            /* Query Explanation
            * Updating breeding with no of tadpoles created */
            $strQuery   = ' UPDATE frog_breeding AS B
                            INNER JOIN
                                (
                                    SELECT breeding_id, count(*) AS tadpoleCnt FROM frog_master
                                    WHERE now_as = "TADPOLE" AND is_alive = "Y"
                                    GROUP BY breeding_id
                                ) AS F
                            ON
                                F.breeding_id = B.breeding_id
                            SET
                                no_of_tadpoles = F.tadpoleCnt ' ;
            DB::update( $strQuery ) ;

            /* Query Explanation
            * Updating no.of tadpole & healthy tadpole(%) in pond master from frog master table */
            $strQuery   = ' UPDATE pond_master AS P
                            INNER JOIN
                                (
                                    SELECT
                                        pond_id, COUNT( frog_id ) AS no_of_tadpoles,
                                        SUM( IF( healthy = "Y", 1, 0 ) ) AS healthy
                                    FROM frog_master
                                    WHERE now_as = "TADPOLE" AND is_alive = "Y"
                                    GROUP BY pond_id
                                ) AS F
                            ON
                                P.pond_id = F.pond_id
                            SET
                                P.no_of_tadpoles = F.no_of_tadpoles,
                                P.healthy_tadpole = ( F.healthy / F.no_of_tadpoles * 100 ) ' ;
            DB::update( $strQuery ) ;

            /* Query Explanation
            * Updating no.of frog & healthy frog(%) in pond master from frog master table */
            $strQuery   = ' UPDATE pond_master AS P
                            INNER JOIN
                                (
                                    SELECT
                                        pond_id, COUNT( frog_id ) AS no_of_frogs,
                                        SUM( IF( healthy = "Y", 1, 0 ) ) AS healthy
                                    FROM frog_master
                                    WHERE now_as = "FROG" AND is_alive = "Y"
                                    GROUP BY pond_id
                                ) AS F
                            ON
                                P.pond_id = F.pond_id
                            SET
                                P.no_of_frogs = F.no_of_frogs,
                                P.healthy_frog = ( F.healthy / F.no_of_frogs * 100 ) ' ;
            DB::update( $strQuery ) ;

            /* Query Explanation
            * Updating avg tadpole to frog(%) in pond master from frog master table */
            $strQuery   = ' UPDATE pond_master AS P
                            INNER JOIN
                                (
                                    SELECT
                                        pond_id, COUNT( frog_id ) AS totalCnt,
                                        SUM( IF( now_as = "FROG", 1, 0 ) ) AS frogCnt
                                    FROM frog_master
                                    GROUP BY pond_id
                                ) AS F
                            ON
                                P.pond_id = F.pond_id
                            SET
                                P.avg_tadpole_to_frog = ( F.frogCnt / F.totalCnt * 100 ) ' ;
            DB::update( $strQuery ) ;
        }
        catch( Exception $e ) {

            Log::debug( 'Frog::dataIntegrity() - ' . $e->getMessage() ) ;
            return false ;
        }

    }   // End of Frog::dataIntegrity()


}   // End of class - Frog { }
