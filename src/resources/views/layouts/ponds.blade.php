@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="pondListRow">

                    @if ( $ponds->total() )
                        <h2>Ponds</h2>
                        <ul class="list-unstyled">
							<table class="table" style="font-family: verdana">
								<tr>
									<th>Pond ID</th>
									<th>Capacity</th>
									<th>Eggs</th>
									<th>Tadpoles</th>
									<th>Frogs</th>
									<th>Updated At</th>
								</tr>

							@foreach( $ponds as $pond )

								<tr>
									<td><a href="{{ route( 'ponddetail', $pond->pond_id ) }}">{{ $pond->pond_id }}</a></td>
									<td>{{ $pond->pond_capacity }}</td>
									<td>{{ $pond->no_of_eggs }}</td>
									<td>{{ $pond->no_of_tadpoles }}</td>
									<td>{{ $pond->no_of_frogs }}</td>
									@php
										$objUpdatedDate = new DateTime( $pond->updated_at ) ;
									@endphp
									<td>{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}</td>
								</tr>

							@endforeach

							</table>

							<li>{{ $ponds->links() }}</li>
                        </ul>
					@else
						No pond available
                    @endif
                </div>
            </div>
        </div>
    @endsection
