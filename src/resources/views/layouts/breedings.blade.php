@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="frogListRow">

                    @if ( $breedings->total() )
                        <h2>Breedings</h2>
                        <ul class="list-unstyled">
							<table class="table" style="font-family: verdana">
								<tr>
									<th>Breeding ID</th>
									<th>Pond ID</th>
									<th>Date of Lay</th>
									<th>No. of Eggs</th>
									<th>Egg Developed</th>
									<th>Updated At</th>
								</tr>
							@foreach( $breedings as $breeding )

								<tr>
									<td><a href="{{ route( 'breedingdetail', $breeding->breeding_id ) }}">{{ $breeding->breeding_id }}</a></td>
									<td>{{ $breeding->pond_id }}</td>
									<td>
										@php
											$objDol = new DateTime( $breeding->date_of_lay ) ;
										@endphp
										{{ $objDol->format( 'd-M-Y - H:i' ) }}
									</td>
									<td>{{ $breeding->no_of_eggs }}</td>
									<td>{{ ( 'Y' == $breeding->egg_developed ) ? 'Yes' : 'No' }}</td>
									<td>
										@php
											$objUpdatedDate = new DateTime( $breeding->updated_at ) ;
										@endphp
										{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}
									</td>
								</tr>

							@endforeach

							</table>

							<li>{{ $breedings->links() }}</li>
                        </ul>
					@else
						No Breeding available
                    @endif
                </div>
            </div>
        </div>
    @endsection
