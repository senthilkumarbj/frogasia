@extends('layouts/app')
    @section('topMenu')

    @endsection

	@section('content')

		<div class="row ">
			<div class="col-md-12 col-sm-12 col-xs-12">
				FrogAsia Technical Test<br /><br />
				This tech test will help us evaluate your skills.<br /><br />
				There is no time limit attached to this test, so take your time and be thorough.<br /><br />
            </div>
        </div>

    @endsection
