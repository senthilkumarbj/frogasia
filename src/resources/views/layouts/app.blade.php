@include("layouts.partials.header")
	<section class="centerRow">
        <div class="col-sm-12 col-md-12 col-xs-12 topMenu">
            <ul class="list-unstyled list-inline">
                @yield('topMenu')
                <li><a href="{{route('home')}}" class="{{ (Request::is('/')) ? 'active' : ''}}" >Home</a></li>
				<li> | </li>
                <li><a href="{{route('frogs')}}" class="{{ (Request::is('frogs')) ? 'active' : ''}}"  >Frogs</a></li>
				<li> | </li>
                <li><a href="{{route('ponds')}}" class="{{ (Request::is('ponds')) ? 'active' : ''}}" >Ponds</a></li>
				<li> | </li>
                <li><a href="{{route('breedings')}}" class="{{ (Request::is('breedings')) ? 'active' : ''}}" >Breedings</a></li>
				<li> | </li>
                <li><a href="{{route('makeBreeding')}}" class="{{ (Request::is('breeding/make')) ? 'active' : ''}}" >Make Breeding</a></li>
				<li> | </li>
                <li><a href="{{route('frogBirth')}}" class="{{ (Request::is('frog/birth')) ? 'active' : ''}}" >Tadpole Birth</a></li>
				<li> | </li>
                <li><a href="{{route('tadpoleDevelopment')}}" class="{{ (Request::is('tadpole/development')) ? 'active' : ''}}" >Make Frog</a></li>
				<li> | </li>
                <li> <a href="{{route('adultDevelopment')}}" class="{{ (Request::is('frog/adultdevelopment')) ? 'active' : ''}}" >Make Adult</a> </li>
				<li> | </li>
                <li> <a href="{{route('frogInfection')}}" class="{{ (Request::is('frog/infection')) ? 'active' : ''}}" >Make Infection</a> </li>
				<li> | </li>
                <li> <a href="{{route('frogDeath')}}" class="{{ (Request::is('frog/death')) ? 'active' : ''}}" >Make Death</a> </li>
				<li> | </li>
                <li> <a href="{{route('pondCleaning')}}" class="{{ (Request::is('pond/cleaning')) ? 'active' : ''}}" >Pond Cleaning</a> </li>
            </ul>
        </div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
			        @yield('content')
				</div>
			</div>
        </div>
	</section>
@include("layouts.partials.footer")
