@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="frogListRow">

                    @if ( $frogs->total() )
                        <h2>Ponds</h2>
                        <ul class="list-unstyled">
							<table class="table" style="font-family: verdana">
								<tr>
									<th>Frog ID</th>
									<th>Pond ID</th>
									<th>Name</th>
									<th>Gender</th>
									<th>Alive</th>
									<th>Ready to Breeding</th>
									<th>Updated At</th>
								</tr>

							@foreach( $frogs as $frog )

								<tr>
									<td><a href="{{ route( 'frogdetail', $frog->frog_id ) }}">{{ $frog->frog_id }}</a></td>
									<td>{{ $frog->pond_id }}</td>
									<td>{{ $frog->frog_name }}</td>
									<td>{{ ( 'M' == $frog->frog_gender ) ? 'Male' : 'Female' }}</td>
									<td>{{ ( 'Y' == $frog->is_alive ) ? 'Yes' : 'No' }}</td>
									<td>{{ ( 'Y' == $frog->ready_to_breed ) ? 'Yes' : 'No' }}</td>
									@php
										$objUpdatedDate = new DateTime( $frog->updated_at ) ;
									@endphp
									<td>{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}</td>
								</tr>

							@endforeach

							</table>

							<li>{{ $frogs->links() }}</li>
                        </ul>
					@else
						No frog available
                    @endif
                </div>
            </div>
        </div>
    @endsection
