@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
				@if ( $frog )
					<h2>Frog Details</h2>
					<table class="table" style="font-family: verdana; width: 40%">
						<tr>
							<th>Frog Id</th>
							<td>{{ $frog->frog_id }}</td>
						</tr>
						<tr>
							<th>Pond Id</th>
							<td>{{ $frog->pond_id }}</td>
						</tr>
						<tr>
							<th>Breeding Id</th>
							<td>{{ $frog->breeding_id }}</td>
						</tr>

						<tr>
							<th>Name</th>
							<td>{{ $frog->frog_name }}</td>
						</tr>
						<tr>
							<th>Gender</th>
							<td>{{ ( 'M' == $frog->frog_gender ) ? 'Male' : 'Female' }}</td>
						</tr>
						<tr>
							<th>Date of Birth</th>
							<td>
								@php
									$objDob = new DateTime( $frog->date_of_birth ) ;
								@endphp
								{{ $objDob->format( 'd-M-Y - H:i' ) }}
							</td>
						</tr>

					@if ( 'Y' == $frog->is_alive )
						<tr>
							<th>Alive</th>
							<td>Yes</td>
						</tr>
					@else
						<tr>
							<th>Alive</th>
							<td>No</td>
						</tr>
						<tr>
							<th>Date of Death</th>
							<td>
								@php
									$objDod = new DateTime( $frog->date_of_death ) ;
								@endphp
								{{ $objDod->format( 'd-M-Y - H:i' ) }}
							</td>
						</tr>
						<tr>
							<th>Cause of Death</th>
							<td>{{ $frog->cause_of_death }}</td>
						</tr>
					@endif

					@if ( 'TADPOLE' == $frog->now_as )
						<tr>
							<th>Now as</th>
							<td>Tadpole</td>
						</tr>
					@else
						<tr>
							<th>Now as</th>
							<td>Frog</td>
						</tr>
						<tr>
							<th>Date of Develop to Tadpole</th>
							<td>
								@php
									$objDodt = new DateTime( $frog->dod_tadpole ) ;
								@endphp
								{{ $objDodt->format( 'd-M-Y - H:i' ) }}
							</td>
						</tr>
					@endif

						<tr>
							<th>Healthy</th>
							<td>{{ ( 'Y' == $frog->healthy ) ? 'Yes' : 'No' }}</td>
						</tr>
						<tr>
							<th>Ready to Breeding</th>
							<td>{{ ( 'Y' == $frog->ready_to_breed ) ? 'Yes' : 'No' }}</td>
						</tr>

						<tr>
							<th>Created Date</th>
							<td>
								@php
									$objCreatedDate = new DateTime( $frog->created_at ) ;
								@endphp
								<div>{{ $objCreatedDate->format( 'd-M-Y' ) }}</div>
							</td>
						</tr>
						<tr>
							<th>Update Date</th>
							<td>
								@php
									$objUpdatedDate = new DateTime( $frog->updated_at ) ;
								@endphp
								<div>{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}</div>
							</td>
						</tr>
					</table>
				@else
					No frog available
				@endif
            </div>
        </div>

    @endsection
