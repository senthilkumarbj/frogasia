@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="pondRow">
                    @if ( $pond )
                        <h2>Pond Details</h2>
						<table class="table" style="font-family: verdana; width: 40%">
							<tr>
								<th>Pond Id</th>
								<td>{{ $pond->pond_id }}</td>
							</tr>

							<tr>
								<th>Average Length</th>
								<td>{{ $pond->avg_length }}</td>
							</tr>
							<tr>
								<th>Average Width</th>
								<td>{{ $pond->avg_width }}</td>
							</tr>
							<tr>
								<th>Average Depth</th>
								<td>{{ $pond->avg_depth }}</td>
							</tr>

							<tr>
								<th>Pond Capacity</th>
								<td>{{ $pond->pond_capacity }}</td>
							</tr>
							<tr>
								<th>Maintenance Cost</th>
								<td>{{ $pond->maintenance_cost }}</td>
							</tr>
							<tr>
								<th>Last Cleaned Date</th>
								<td>
									@if ( $pond->last_cleaned_date )
										@php
											$objCleanedDate = new DateTime( $pond->last_cleaned_date ) ;
										@endphp
										<div>{{ $objCleanedDate->format( 'd-M-Y - H:i' ) }}</div>
									@else
										Not Yet Cleaned
									@endif
									{{ $pond->last_cleaned_date }}
								</td>
							</tr>

							<tr>
								<th>No. of Eggs</th>
								<td>{{ $pond->no_of_eggs }}</td>
							</tr>
							<tr>
								<th>No. of Tadpoles</th>
								<td>{{ $pond->no_of_tadpoles }}</td>
							</tr>
							<tr>
								<th>No. of Frogs</th>
								<td>{{ $pond->no_of_frogs }}</td>
							</tr>
							<tr>
								<th>Total Count</th>
								<td>{{ $pond->total_count }}</td>
							</tr>

							<tr>
								<th>Average Life of Frog</th>
								<td>{{ $pond->avg_life_frog }}</td>
							</tr>
							<tr>
								<th>Average Life of Tadpoles</th>
								<td>{{ $pond->avg_life_tadpole }}</td>
							</tr>
							<tr>
								<th>Average Egg to Tadpoles</th>
								<td>{{ $pond->avg_egg_to_tadpole }}</td>
							</tr>
							<tr>
								<th>Average Tadpoles to Frogs</th>
								<td>{{ $pond->avg_tadpole_to_frog }}</td>
							</tr>

							<tr>
								<th>Healthy Frogs</th>
								<td>{{ $pond->healthy_frog }}</td>
							</tr>
							<tr>
								<th>Healthy Tadpoles</th>
								<td>{{ $pond->healthy_tadpole }}</td>
							</tr>
							<tr>
								<th>Created Date</th>
								<td>
									@php
										$objCreatedDate = new DateTime( $pond->created_at ) ;
									@endphp
									<div>{{ $objCreatedDate->format( 'd-M-Y' ) }}</div>
								</td>
							</tr>
							<tr>
								<th>Update Date</th>
								<td>
									@php
										$objUpdatedDate = new DateTime( $pond->updated_at ) ;
									@endphp
									<div>{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}</div>
								</td>
							</tr>
						</table>
                    @else
                        No pond available
                    @endif
                </div>
            </div>
        </div>

    @endsection
