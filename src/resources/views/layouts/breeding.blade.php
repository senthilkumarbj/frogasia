@extends('layouts/app')
	@section('content')
		<div class="row " >
			<div class="col-md-12 col-sm-12 col-xs-12" >
				@if ( $breeding )
					<h2>Breeding Details</h2>
					<table class="table" style="font-family: verdana; width: 40%">
						<tr>
							<th>Breeding Id</th>
							<td>{{ $breeding->breeding_id }}</td>
						</tr>
						<tr>
							<th>Pond Id</th>
							<td>{{ $breeding->pond_id }}</td>
						</tr>

						<tr>
							<th>Male Frog Id</th>
							<td>{{ $breeding->male_frog_id }}</td>
						</tr>
						<tr>
							<th>Female Frog Id</th>
							<td>{{ $breeding->female_frog_id }}</td>
						</tr>

						<tr>
							<th>Date of Lay</th>
							<td>
								@php
									$objDol = new DateTime( $breeding->date_of_lay ) ;
								@endphp
								<div>{{ $objDol->format( 'd-M-Y' ) }}</div>
							</td>
						</tr>
						<tr>
							<th>No. of Egg</th>
							<td>{{ $breeding->no_of_eggs }}</td>
						</tr>

						<tr>
							<th>Egg Developed to Tadpole</th>
							<td>{{ ( 'Y' == $breeding->egg_developed ) ? 'Yes' : 'No' }}</td>
						</tr>
						<tr>
							<th>No. of Tadpole</th>
							<td>{{ $breeding->no_of_tadpoles }}</td>
						</tr>

						<tr>
							<th>Created Date</th>
							<td>
								@php
									$objCreatedDate = new DateTime( $breeding->created_at ) ;
								@endphp
								<div>{{ $objCreatedDate->format( 'd-M-Y - H:i' ) }}</div>
							</td>
						</tr>
						<tr>
							<th>Update Date</th>
							<td>
								@php
									$objUpdatedDate = new DateTime( $breeding->updated_at ) ;
								@endphp
								<div>{{ $objUpdatedDate->format( 'd-M-Y - H:i' ) }}</div>
							</td>
						</tr>
					</table>
				@else
					Breeding details not available
				@endif
            </div>
        </div>

    @endsection
