
INSERT INTO pond_master (pond_id, avg_length, avg_width, avg_depth, pond_capacity, maintenance_cost, created_at, updated_at) VALUES
(1, 1000, 1000, 50, 5000000, '10000.00',  '2016-11-08 19:42:09', '2016-11-08 19:42:09'),
(2, 2000, 2000, 80, 16000000, '25000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00'),
(3, 2340, 2560, 150, 56600000, '65000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00'),
(4, 4350, 5760, 270, 83700000, '85000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00');

INSERT INTO frog_master ( pond_id, frog_name, frog_gender, date_of_birth, date_of_death, cause_of_death, is_alive, now_as, dod_tadpole, healthy, ready_to_breed, created_at, updated_at) VALUES
( 3, 'm4', 'M', '2012-11-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:15', '2016-11-08 19:46:15'),
( 4, 'f4', 'F', '2012-12-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-02-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:48', '2016-11-08 19:46:48'),
( 1, 'm1', 'M', '2010-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2010-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'f1', 'F', '2007-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'm2', 'M', '2011-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'f2', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 4, 'm3', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f3', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 4, 'm10', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f10', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),

( 3, 'm5', 'M', '2012-11-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:15', '2016-11-08 19:46:15'),
( 4, 'f5', 'F', '2012-12-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-02-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:48', '2016-11-08 19:46:48'),
( 4, 'm6', 'M', '2010-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2010-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 3, 'f6', 'F', '2007-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'm7', 'M', '2011-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'f7', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'm8', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 2, 'f8', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 4, 'm9', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f9', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 1, 'f2', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27');
