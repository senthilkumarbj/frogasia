FROGASIA ASSESSMENT
-------- ----------

SOFTWARES
---------
    * Apache 2.4.23
    * PHP 5.6.25
    * MySQL 5.7.16
    * Composer 1.2.2
    * Laravel 5.3

SOFTWARE JUSTIFICATION
-------- -------------
    Why Laravel :
        * Controllers and RESTful Resource Controllers
        * Authentication
        * Routing system
        * Unit-testing
        * Database: Migrations and Seeding
        * Eloquent ORM
        * Query Builder
        * HTTP Middleware
        * Support Blade Templates
        * Artisan Console
        * Caching
        * Filesystem / Cloud Storage
        * Very well Documented
        * Class auto loading
        * Easy to work and begin
        * MVC is very flexible
        * Fluent syntax
        * Modularity
        * It is an Open Source

INSTALLATION INSTRUCTIONS
------------ ------------
    * Install Apache, PHP, MySQL, Composer softwares
    * Create project folder, extract source file in it. ( create virtual hosts file and point public folder)
    * Create virtual host entry for this project
    * Run below commands
        - chmod 777 -R storage
        - chmod 777 -R bootstrap/cache
    * Create frogasia database, and run the below mentioned files
        - 1_DB_structure.sql - It has crate table & triggers queries
        - 2_sample_data.sql - It has some sample data queries
	* Change setting in .env file, update below variables
        - DB_CONNECTION=mysql
        - DB_HOST=127.0.0.1
        - DB_PORT=3306
        - DB_DATABASE=frog
        - DB_USERNAME=root
        - DB_PASSWORD=
	* Run composer update command
        - composer update
	* Run test cases : phpunit

NOTE
----
    * 3_make_data_query.sql ( It has some query to make records for )
        - frog / tadpole birth,
        - tadpole to frog development,
        - frog adult development
