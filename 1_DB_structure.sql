/*
 * Malay / Chinese / Tamil / English - To support multi languages, I choosen utf8mb4
 */

DROP DATABASE IF EXISTS frogasia ;

CREATE DATABASE frogasia CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;


DROP TABLE IF EXISTS frog_master;
CREATE TABLE frog_master (
    frog_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pond_id BIGINT UNSIGNED  DEFAULT '0',
    breeding_id BIGINT UNSIGNED  DEFAULT '0',

    frog_name VARCHAR(255) NULL,
    frog_gender ENUM("M","F") NULL,
    date_of_birth DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,

    date_of_death DATETIME NULL,
    cause_of_death VARCHAR(255) NULL,
    is_alive ENUM("Y","N") NOT NULL DEFAULT "Y",

    now_as ENUM("TADPOLE","FROG") NOT NULL DEFAULT "TADPOLE" COMMENT "current status of frog, like he is child or men",
    dod_tadpole DATETIME NULL COMMENT "Date of development to Tadpole from egg",

    healthy ENUM("Y","N") NOT NULL DEFAULT "Y",
    ready_to_breed ENUM("Y","N") NOT NULL DEFAULT "N",

    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (frog_id),
    KEY frog_master_IK_breeding_id (breeding_id),
    KEY frog_master_IK_pond_id (pond_id),
    KEY frog_master_IK_gender (frog_gender),

    KEY frog_master_IK_cause_of_death (cause_of_death),
    KEY frog_master_IK_is_alive (is_alive),
    KEY frog_master_IK_now_as (now_as),
    KEY frog_master_IK_healthy (healthy),
    KEY frog_master_IK_ready_to_breed (ready_to_breed)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS frog_breeding;
CREATE TABLE frog_breeding (
    breeding_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pond_id BIGINT UNSIGNED NOT NULL DEFAULT '0',
    male_frog_id BIGINT UNSIGNED  DEFAULT '0',
    female_frog_id BIGINT UNSIGNED  DEFAULT '0',
    date_of_lay DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    no_of_eggs SMALLINT UNSIGNED NOT NULL DEFAULT '0',
    no_of_tadpoles SMALLINT UNSIGNED DEFAULT '0',
    egg_developed ENUM("Y","N") NOT NULL DEFAULT "N" COMMENT "whether egg(s) convertion or frog birth of this breed is done or not",
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (breeding_id),
    KEY frog_breeding_IK_male_frog_id (male_frog_id),
    KEY frog_breeding_IK_female_frog_id (female_frog_id),
    KEY frog_breeding_IK_egg_developed (egg_developed)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS pond_master;
CREATE TABLE pond_master (
    pond_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT "Primary Key",
    avg_length INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    avg_width INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    avg_depth INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    pond_capacity BIGINT UNSIGNED NOT NULL DEFAULT '0' COMMENT "capacity of pond with frogs, tadpoles & eggs",
    maintenance_cost decimal(12,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT "per month",
    last_cleaned_date DATETIME NULL DEFAULT NULL,
    no_of_eggs INT UNSIGNED DEFAULT '0' COMMENT "current count of eggs",
    no_of_tadpoles INT UNSIGNED DEFAULT '0' COMMENT "current count of tadpoles",
    no_of_frogs INT UNSIGNED DEFAULT '0' COMMENT "current count of frogs",
    total_count BIGINT UNSIGNED DEFAULT '0' COMMENT "total count of frogs + tadpoles + eggs",
    avg_egg_to_tadpole decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    avg_tadpole_to_frog decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    healthy_frog decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    healthy_tadpole decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (pond_id),
    KEY pond_master_IK_maintenance_cost (maintenance_cost),
    KEY pond_master_IK_pond_capacity (pond_capacity),
    KEY pond_master_IK_no_of_eggs (no_of_eggs),
    KEY pond_master_IK_no_of_tadpoles (no_of_tadpoles),
    KEY pond_master_IK_no_of_frogs (no_of_frogs)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS pond_master_triggerBeforeInsert;
DELIMITER $$
CREATE TRIGGER pond_master_triggerBeforeInsert
    BEFORE INSERT ON pond_master  FOR EACH ROW
    SET
        NEW.total_count =  NEW.no_of_eggs + NEW.no_of_tadpoles + NEW.no_of_frogs  ;
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS pond_master_triggerBeforeUpdate;
DELIMITER $$
CREATE TRIGGER pond_master_triggerBeforeUpdate
    BEFORE UPDATE ON pond_master  FOR EACH ROW
    SET
        NEW.total_count =  NEW.no_of_eggs + NEW.no_of_tadpoles + NEW.no_of_frogs  ;
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS frog_breeding_triggerAfterInsert;
DELIMITER $$
CREATE TRIGGER frog_breeding_triggerAfterInsert
    AFTER INSERT ON frog_breeding FOR EACH ROW
    BEGIN
        UPDATE pond_master AS P
            INNER JOIN
                (
                    SELECT pond_id, SUM( no_of_eggs ) AS no_of_eggs FROM frog_breeding
                    WHERE pond_id = NEW.pond_id AND egg_developed = "N"
                    GROUP BY pond_id
                ) AS B
            ON P.pond_id = B.pond_id
            SET P.no_of_eggs = B.no_of_eggs ;

        UPDATE frog_master SET updated_at = NOW() WHERE frog_id IN ( NEW.male_frog_id, NEW.female_frog_id ) ;
    END
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS frog_breeding_triggerAfterUpdate;
DELIMITER $$
CREATE TRIGGER frog_breeding_triggerAfterUpdate
    AFTER UPDATE ON frog_breeding FOR EACH ROW
    BEGIN
        UPDATE pond_master AS P
            INNER JOIN
                (
                    SELECT
                        pond_id, SUM( no_of_eggs ) AS no_of_eggs, SUM( no_of_tadpoles ) AS no_of_tadpoles
                    FROM frog_breeding
                    WHERE pond_id = NEW.pond_id AND egg_developed = "Y"
                    GROUP BY pond_id
                ) AS B
            ON P.pond_id = B.pond_id
            SET P.avg_egg_to_tadpole = ( B.no_of_tadpoles / B.no_of_eggs * 100 ) ;
    END
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------
