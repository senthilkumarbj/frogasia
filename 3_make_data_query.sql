
/* Query to prepare to make records for egg development or frog birth */
UPDATE frog_breeding SET
    date_of_lay = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 10 DAY ),
                                    DATE_SUB( NOW(), INTERVAL 7 DAY )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 10 DAY )
                )
    WHERE egg_developed = 'N'  LIMIT 4 ;

/* Query to prepare to make records for tadpole development */
UPDATE frog_master SET
    date_of_birth = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 110 DAY ),
                                    DATE_SUB( NOW(), INTERVAL 100 DAY )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 110 DAY )
                )
WHERE
    is_alive = 'Y' AND healthy = 'Y' AND now_as = 'TADPOLE' LIMIT 12 ;

/* Query to prepare to make records for frog as adult */
UPDATE frog_master SET
    date_of_birth = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 45 MONTH ),
                                    DATE_SUB( NOW(), INTERVAL 40 MONTH )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 45 MONTH )
                )
WHERE
    is_alive = 'Y' AND healthy = 'Y' AND now_as = 'FROG' AND ready_to_breed = 'N' LIMIT 12 ;
