/*
 * Malay / Chinese / Tamil / English - To support multi languages, I choosen utf8mb4
 */

DROP DATABASE IF EXISTS frogasia ;

CREATE DATABASE frogasia CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;


DROP TABLE IF EXISTS frog_master;
CREATE TABLE frog_master (
    frog_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pond_id BIGINT UNSIGNED  DEFAULT '0',
    breeding_id BIGINT UNSIGNED  DEFAULT '0',

    frog_name VARCHAR(255) NULL,
    frog_gender ENUM("M","F") NULL,
    date_of_birth DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,

    date_of_death DATETIME NULL,
    cause_of_death VARCHAR(255) NULL,
    is_alive ENUM("Y","N") NOT NULL DEFAULT "Y",

    now_as ENUM("TADPOLE","FROG") NOT NULL DEFAULT "TADPOLE" COMMENT "current status of frog, like he is child or men",
    dod_tadpole DATETIME NULL COMMENT "Date of development to Tadpole from egg",

    healthy ENUM("Y","N") NOT NULL DEFAULT "Y",
    ready_to_breed ENUM("Y","N") NOT NULL DEFAULT "N",

    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (frog_id),
    KEY frog_master_IK_breeding_id (breeding_id),
    KEY frog_master_IK_pond_id (pond_id),
    KEY frog_master_IK_gender (frog_gender),

    KEY frog_master_IK_cause_of_death (cause_of_death),
    KEY frog_master_IK_is_alive (is_alive),
    KEY frog_master_IK_now_as (now_as),
    KEY frog_master_IK_healthy (healthy),
    KEY frog_master_IK_ready_to_breed (ready_to_breed)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS frog_breeding;
CREATE TABLE frog_breeding (
    breeding_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pond_id BIGINT UNSIGNED NOT NULL DEFAULT '0',
    male_frog_id BIGINT UNSIGNED  DEFAULT '0',
    female_frog_id BIGINT UNSIGNED  DEFAULT '0',
    date_of_lay DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    no_of_eggs SMALLINT UNSIGNED NOT NULL DEFAULT '0',
    no_of_tadpoles SMALLINT UNSIGNED DEFAULT '0',
    egg_developed ENUM("Y","N") NOT NULL DEFAULT "N" COMMENT "whether egg(s) convertion or frog birth of this breed is done or not",
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (breeding_id),
    KEY frog_breeding_IK_male_frog_id (male_frog_id),
    KEY frog_breeding_IK_female_frog_id (female_frog_id),
    KEY frog_breeding_IK_egg_developed (egg_developed)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS pond_master;
CREATE TABLE pond_master (
    pond_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT "Primary Key",
    avg_length INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    avg_width INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    avg_depth INT UNSIGNED NOT NULL DEFAULT '0' COMMENT "in metre",
    pond_capacity BIGINT UNSIGNED NOT NULL DEFAULT '0' COMMENT "capacity of pond with frogs, tadpoles & eggs",
    maintenance_cost decimal(12,2) UNSIGNED NOT NULL DEFAULT '0' COMMENT "per month",
    last_cleaned_date DATETIME NULL DEFAULT NULL,
    no_of_eggs INT UNSIGNED DEFAULT '0' COMMENT "current count of eggs",
    no_of_tadpoles INT UNSIGNED DEFAULT '0' COMMENT "current count of tadpoles",
    no_of_frogs INT UNSIGNED DEFAULT '0' COMMENT "current count of frogs",
    total_count BIGINT UNSIGNED DEFAULT '0' COMMENT "total count of frogs + tadpoles + eggs",
    avg_egg_to_tadpole decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    avg_tadpole_to_frog decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    healthy_frog decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    healthy_tadpole decimal(5,2) UNSIGNED DEFAULT '0' COMMENT "in percentage",
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (pond_id),
    KEY pond_master_IK_maintenance_cost (maintenance_cost),
    KEY pond_master_IK_pond_capacity (pond_capacity),
    KEY pond_master_IK_no_of_eggs (no_of_eggs),
    KEY pond_master_IK_no_of_tadpoles (no_of_tadpoles),
    KEY pond_master_IK_no_of_frogs (no_of_frogs)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS pond_master_triggerBeforeInsert;
DELIMITER $$
CREATE TRIGGER pond_master_triggerBeforeInsert
    BEFORE INSERT ON pond_master  FOR EACH ROW
    SET
        NEW.total_count =  NEW.no_of_eggs + NEW.no_of_tadpoles + NEW.no_of_frogs  ;
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS pond_master_triggerBeforeUpdate;
DELIMITER $$
CREATE TRIGGER pond_master_triggerBeforeUpdate
    BEFORE UPDATE ON pond_master  FOR EACH ROW
    SET
        NEW.total_count =  NEW.no_of_eggs + NEW.no_of_tadpoles + NEW.no_of_frogs  ;
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS frog_breeding_triggerAfterInsert;
DELIMITER $$
CREATE TRIGGER frog_breeding_triggerAfterInsert
    AFTER INSERT ON frog_breeding FOR EACH ROW
    BEGIN
        UPDATE pond_master AS P
            INNER JOIN
                (
                    SELECT pond_id, SUM( no_of_eggs ) AS no_of_eggs FROM frog_breeding
                    WHERE pond_id = NEW.pond_id AND egg_developed = "N"
                    GROUP BY pond_id
                ) AS B
            ON P.pond_id = B.pond_id
            SET P.no_of_eggs = B.no_of_eggs ;

        UPDATE frog_master SET updated_at = NOW() WHERE frog_id IN ( NEW.male_frog_id, NEW.female_frog_id ) ;
    END
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS frog_breeding_triggerAfterUpdate;
DELIMITER $$
CREATE TRIGGER frog_breeding_triggerAfterUpdate
    AFTER UPDATE ON frog_breeding FOR EACH ROW
    BEGIN
        UPDATE pond_master AS P
            INNER JOIN
                (
                    SELECT
                        pond_id, SUM( no_of_eggs ) AS no_of_eggs, SUM( no_of_tadpoles ) AS no_of_tadpoles
                    FROM frog_breeding
                    WHERE pond_id = NEW.pond_id AND egg_developed = "Y"
                    GROUP BY pond_id
                ) AS B
            ON P.pond_id = B.pond_id
            SET P.avg_egg_to_tadpole = ( B.no_of_tadpoles / B.no_of_eggs * 100 ) ;
    END
$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------

INSERT INTO pond_master (pond_id, avg_length, avg_width, avg_depth, pond_capacity, maintenance_cost, created_at, updated_at) VALUES
(1, 1000, 1000, 50, 5000000, '10000.00',  '2016-11-08 19:42:09', '2016-11-08 19:42:09'),
(2, 2000, 2000, 80, 16000000, '25000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00'),
(3, 2340, 2560, 150, 56600000, '65000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00'),
(4, 4350, 5760, 270, 83700000, '85000.00',  '2016-11-08 19:43:00', '2016-11-08 19:43:00');

INSERT INTO frog_master ( pond_id, frog_name, frog_gender, date_of_birth, date_of_death, cause_of_death, is_alive, now_as, dod_tadpole, healthy, ready_to_breed, created_at, updated_at) VALUES
( 3, 'm4', 'M', '2012-11-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:15', '2016-11-08 19:46:15'),
( 4, 'f4', 'F', '2012-12-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-02-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:48', '2016-11-08 19:46:48'),
( 1, 'm1', 'M', '2010-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2010-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'f1', 'F', '2007-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'm2', 'M', '2011-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'f2', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 4, 'm3', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f3', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 4, 'm10', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f10', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),

( 3, 'm5', 'M', '2012-11-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:15', '2016-11-08 19:46:15'),
( 4, 'f5', 'F', '2012-12-08 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-02-08 04:11:15', 'Y', 'Y', '2016-11-08 19:46:48', '2016-11-08 19:46:48'),
( 4, 'm6', 'M', '2010-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2010-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 3, 'f6', 'F', '2007-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:50:23', '2016-11-09 09:50:23'),
( 2, 'm7', 'M', '2011-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'f7', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27'),
( 1, 'm8', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2011-10-19 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 2, 'f8', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'N', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 4, 'm9', 'M', '2014-07-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2013-01-08 04:11:15', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 3, 'f9', 'F', '2015-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2007-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:52:11', '2016-11-09 09:52:11'),
( 1, 'f2', 'F', '2012-06-09 00:00:00', NULL, NULL, 'Y', 'FROG', '2012-09-23 00:00:00', 'Y', 'Y', '2016-11-09 09:51:27', '2016-11-09 09:51:27');

-- ---------------------------------------------------------------------------------------------------------------------




-- ---------------------------------------------------------------------------------------------------------------------

/* Query to prepare to make records for egg development or frog birth */
UPDATE frog_breeding SET
    date_of_lay = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 10 DAY ),
                                    DATE_SUB( NOW(), INTERVAL 7 DAY )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 10 DAY )
                )
    WHERE egg_developed = 'N'  LIMIT 4 ;

/* Query to prepare to make records for tadpole development */
UPDATE frog_master SET
    date_of_birth = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 110 DAY ),
                                    DATE_SUB( NOW(), INTERVAL 100 DAY )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 110 DAY )
                )
WHERE
    is_alive = 'Y' AND healthy = 'Y' AND now_as = 'TADPOLE' LIMIT 12 ;

/* Query to prepare to make records for frog as adult */
UPDATE frog_master SET
    date_of_birth = TIMESTAMPADD(
                    SECOND,
                    FLOOR(
                        RAND() * TIMESTAMPDIFF(
                                    SECOND,
                                    DATE_SUB( NOW(), INTERVAL 45 MONTH ),
                                    DATE_SUB( NOW(), INTERVAL 40 MONTH )
                                )
                    ),
                    DATE_SUB( NOW(), INTERVAL 45 MONTH )
                )
WHERE
    is_alive = 'Y' AND healthy = 'Y' AND now_as = 'FROG' AND ready_to_breed = 'N' LIMIT 12 ;
