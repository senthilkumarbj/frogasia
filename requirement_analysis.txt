FROG Details

    The average lifespan of a frog is 6 to 8 years.
    In approximately 12 weeks, they mature into fully developed frogs.
    In 3 years, frogs reach sexual maturity and can reproduce.
    In a week, eggs are develop into tadpoles.
    Tadpoles are left to fend for themselves.
    Approximately five out of every 2,000 tadpoles reach adulthood due to predators and other natural causes
    When breeding season arrives, frogs often gather at ponds or other bodies of water, and some even return to the place where they developed. i.e., Frog needs water while breeding.
    Reproduction
    During amplexus the female discharges eggs -- usually into water -- while the male sheds sperms over the eggs.
    Amplexus can last several days! Usually, it occurs in the water.

------------------------------------------------------------------------------------------------------------------------

POND Details

    Modules for Pond Related information (should be extend this to manage multiple ponds)
     1. Pond maintenance cost per month ( Cleaning, keeping water level )
     2. Pond Dimensions
     3. Number of frogs it has (and then Number of male frogs, number of female frogs)
     4. Number of Tadpoles it has
     5. Average no. of a frog in this pond
     6. Average no. of a tadpole in this pond
     7. Tadpole to Frog development percentage
     8. Tadpole creation percentage
     9. Number of healthy frogs
    10. Number of healthy tadpoles

------------------------------------------------------------------------------------------------------------------------

Modules for Frog related information
    1. Birth
    2. Number of tadpoles it created
    3. Gender
    4. This frog's, tadpole to frog conversion ratio (Like How many tadpoles it created and out of which how many got converted to frog)
    5. Is this frog infected ?
    6. Death date
    7. Reason for the death (will be useful for analytics)

------------------------------------------------------------------------------------------------------------------------

APIs
    1. Breeding / Lay EGG API
    2. Tadpole / Birth API
    3. Ready to Breed API
    4. Death API
    5. Frog API
    6. Infection API
    7. Pond cleaning API

------------------------------------------------------------------------------------------------------------------------
